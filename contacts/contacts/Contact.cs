﻿using System;
using System.Collections.Generic;
using System.Text;

namespace contacts
{
    public class Contact
    {
        private int id;
        private string nom;
        private string prenom;
        private string mail;
        private string numero;
        private string longitude;
        private string latitude;
        public int Id { get => id; set => id = value; }
        public string Nom { get => nom; set => nom = value; }
        public string Prenom { get => prenom; set => prenom = value; }
        public string Mail { get => mail; set => mail = value; }
        public string Numero { get => numero; set => numero = value; }
        public string Longitude { get => longitude; set => longitude = value; }
        public string Latitude { get => latitude; set => latitude = value; }

        public Contact() { }

        public Contact(int id, string nom, string prenom, string mail, string num,string longitude, string latitude) {
            this.Id = id;
            this.Nom = nom;
            this.Numero = num;
            this.Prenom = prenom;
            this.Mail = mail;
            this.Longitude = longitude;
            this.Latitude = latitude;
        }

        public Contact(string nom, string prenom, string mail, string num, string longitude, string latitude) {
            this.Nom = nom;
            this.Numero = num;
            this.Prenom = prenom;
            this.Mail = mail;
            this.Longitude = longitude;
            this.Latitude = latitude;
        }
    }
}
