﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static contacts.MainPage;

namespace contacts
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AllContact : ContentPage
	{
        private ObservableCollection<Contact> lesContacts = new ObservableCollection<Contact>();
        public AllContact (ObservableCollection<Contact> LesContacts)
		{
			InitializeComponent ();
            this.lesContacts = LesContacts;
            ContactView.ItemsSource = this.lesContacts;
            ContactView.ItemSelected += ContactView_ItemSelectedAsync;
        }

        public void saveContacts()
        {
            string toSaveText = "";
            foreach (Contact c in lesContacts)
            {
                toSaveText += c.Nom + ";" + c.Prenom + ";" + c.Mail + ";" + c.Numero + ";" + c.Latitude + ";" + c.Longitude + "\n";
            }
            DependencyService.Get<ISaveAndLoad>().SaveText("temp.txt", toSaveText);
        }

        async void ContactView_ItemSelectedAsync(object sender, SelectedItemChangedEventArgs e)
         {
            var contactClick = (Contact)e.SelectedItem;

            var test = await DisplayAlert("Le contact",
            "Nom : " + contactClick.Nom +
            "\n\rPrénom : " + contactClick.Prenom +
            "\n\rNuméro : " + contactClick.Numero +
            "\n\rAdresse mail : " + contactClick.Mail+
            "\n\rLatitude : " +contactClick.Latitude+
            "\n\rLongitude : " +contactClick.Longitude
            , "Supprimer", "Retour");

            if (test == true)
            {
                foreach (Contact unContact in this.lesContacts)
                {
                    if (contactClick == unContact)
                    {
                        this.lesContacts.Remove(unContact);
                        saveContacts();
                        DisplayAlert("Suppression réussie !", "Le contact à bien était supprimer", "ok");
                        break;
                    }
                }
            }
        }
    }
}