﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Essentials;

namespace contacts
{
    public partial class MainPage : ContentPage
    {
        //API AIzaSyAa5zsgWTPBx3TjZpkDgzoDvGrsyOyW-GQ
        public static ObservableCollection<Contact> lesContacts = new ObservableCollection<Contact>();
        private static double myLatitude;
        private static double myLongitude;
        public MainPage()
        {
            InitializeComponent();
            avoirLocationAsync();
            ContactView.ItemsSource = lesContacts;
            ContactView.ItemSelected += ContactView_ItemSelected;
            this.loadContacts();
        }

        public void resetSaveContacts()
        {
            lesContacts.Clear();
            string toSaveText = "";
            DependencyService.Get<ISaveAndLoad>().SaveText("temp.txt", toSaveText);
        }

        private void ContactView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            DisplayAlert("Alerte", "Vous avez choisi un élément de la liste", "OK");
        }

        void ajoutBTClicked(object sender, EventArgs e)
        {
            Contact c = new Contact(nomContact.Text,prenomContact.Text,mailContact.Text, numeroContact.Text,myLongitude.ToString(),myLatitude.ToString());
            lesContacts.Add(c);
            confirmLabel.Text = "Contact crée"+myLatitude + " - " + myLongitude;
            this.saveContacts();
        }

        public async Task avoirLocationAsync()
        {
            var request = new GeolocationRequest(GeolocationAccuracy.Medium);
            var location = await Geolocation.GetLocationAsync(request);

            if (location != null)
            {
                myLatitude = location.Latitude;
                myLongitude = location.Longitude;
            }
        }

        public void saveContacts() {
            string toSaveText = "";
            foreach (Contact c in lesContacts) {
                toSaveText += c.Nom + ";" + c.Prenom + ";" + c.Mail + ";" + c.Numero + ";" + c.Latitude + ";" + c.Longitude + "\n";
            }
            DependencyService.Get<ISaveAndLoad>().SaveText("temp.txt", toSaveText);
        }

        public void loadContacts()
        {
            String recupere = DependencyService.Get<ISaveAndLoad>().LoadText("temp.txt");

            String[] lesLignesContact = recupere.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            foreach (var item in lesLignesContact) {
                if (item.Length > 0) {
                    string[] leContact = item.Split(';');
                    Contact c = new Contact(leContact[0], leContact[1], leContact[2], leContact[3], leContact[4], leContact[5]);
                    lesContacts.Add(c);
                }
            }
        }

        void OnButtonAideClicked(object sender, EventArgs e) {
            Navigation.PushAsync(new Aide());
        }

        void OnButtonboutonAllContact(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AllContact(lesContacts));
        }

        void OnButtonMap(object sender, EventArgs e)
        {
            Navigation.PushAsync(new map());
        }

        void OnButtonboutonResetAllContact(object sender, EventArgs e)
        {
            resetSaveContacts();
        }

        public interface ISaveAndLoad {
            void SaveText(string filename, string text);
            string LoadText(string filename);
        }
    }
}
